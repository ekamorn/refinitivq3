const http = require('https');
const jsdom = require('jsdom');
const {
  JSDOM
} = jsdom;

const arguments = process.argv;
let query = '';
arguments.forEach((val, index) => {
  if (index == 2) {
    query = val;
  }
});

const options = {
  hostname: 'codequiz.azurewebsites.net',
  path: '/',
  method: 'GET',
  headers: {
    'Cookie': 'hasCookie=true'
  }
};

const results = '';
const req = http.request(options, function(res) {
  let rawHtml = '';

  res.on('data', (chunk) => {
    rawHtml += chunk;
  });
  res.on('end', () => {
    try {
      const dom = new JSDOM(rawHtml);
      const tdList = dom.window.document.querySelectorAll('tr > td');

      tdList.forEach((item, i) => {

        let fundName = item.textContent;
        fundName = fundName.replace(/\s+/g, '');
        if (query == fundName) {
          console.log(tdList[i + 1].textContent);
        }

      });

    } catch (e) {
      console.error(e.message);
    }
  });
});

req.end();
